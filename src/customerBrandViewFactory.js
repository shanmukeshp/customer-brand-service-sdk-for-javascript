import CustomerBrandView from './customerBrandView';

export default class CustomerBrandViewFactory {

    static construct(data):CustomerBrandView {

        const id = data.id;

        const name = data.name;

        const customerSegmentId = data.customerSegmentId;

        return new CustomerBrandView(
            id,
            name,
            customerSegmentId
        );

    }

}

