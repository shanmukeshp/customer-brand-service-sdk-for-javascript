/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'firstName',
    lastName: 'lastName',
    customerBrandId:1,
    customerSegmentId:1,
    customerBrandName:'customerBrandName',
    accountId:'000000000000000000',
    sapVendorNumber:'0000000000',
    userId:'email@test.com',
    url:'https://dummy-url.com'
};