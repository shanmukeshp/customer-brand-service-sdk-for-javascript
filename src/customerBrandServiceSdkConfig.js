/**
 * @class {CustomerBrandServiceSdkConfig}
 */
export default class CustomerBrandServiceSdkConfig {

    _precorConnectApiBaseUrl:string;

    /**
     * @param {string} baseUrl
     */
    constructor(precorConnectApiBaseUrl:string) {

        if (!precorConnectApiBaseUrl) {
            throw 'precorConnectApiBaseUrl required';
        }
        this._precorConnectApiBaseUrl = precorConnectApiBaseUrl;

    }

    /**
     * @returns {string}
     */
    get precorConnectApiBaseUrl():string {
        return this._precorConnectApiBaseUrl;
    }

}